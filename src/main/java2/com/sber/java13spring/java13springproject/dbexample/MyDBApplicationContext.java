package com.sber.java13spring.java13springproject.dbexample;

import com.sber.java13spring.java13springproject.dbexample.dao.UserDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.java13spring.java13springproject.dbexample.constants.DBConsts.*;

@Configuration
@ComponentScan
public class MyDBApplicationContext {

    @Bean
    @Scope("singleton")
    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                                           USER, PASSWORD);
    }
    @Bean
    @Scope("prototype")
    public UserDAO userDAO() throws SQLException {
        return new UserDAO(newConnection());
    }
}
