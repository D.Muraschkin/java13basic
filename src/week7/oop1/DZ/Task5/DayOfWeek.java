package week7.oop1.DZ.Task5;

public class DayOfWeek {
   private  byte n = 0;
   private  String s = "";

    public static DayOfWeek day1 = new DayOfWeek((byte)1,"Monday");
    public static DayOfWeek day2 = new DayOfWeek((byte)2,"Tuesday");
    public static DayOfWeek day3= new DayOfWeek((byte)3,"Wednesday");
    public static DayOfWeek day4 = new DayOfWeek((byte)4,"Thursday");
    public static DayOfWeek day5 = new DayOfWeek((byte)5,"Friday");
    public static DayOfWeek day6 = new DayOfWeek((byte)6,"Saturday");
    public static DayOfWeek day7 = new DayOfWeek((byte) 7,"Sunday");

    DayOfWeek(byte n1, String s1){
        n = n1;
        s = s1;
    }

    public byte getN() {
        return n;
    }

    public String getS() {
        return s;
    }
}
