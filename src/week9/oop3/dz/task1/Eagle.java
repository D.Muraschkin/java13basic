package week10.dz;

public class Eagle implements Bird{

    @Override
    public void eat() {
        Bird.super.eat();
    }

    @Override
    public void sleep() {
        Bird.super.sleep();
    }

    @Override
    public void wayOfBirth() {
       Bird.super.wayOfBirth();
    }

    @Override
    public void methodOfTransportation() {
        System.out.println("летает быстро");
    }
}
