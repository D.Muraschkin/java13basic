package dz.oop2.exception.dz.task1;

public class MyCheckedException extends Exception{

    public MyCheckedException(){
        super("произошла ошибка MyCheckedException");
    }

    public MyCheckedException(String massage){
        super(massage);
    }
}
