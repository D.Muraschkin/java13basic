package week9.oop3.dz.task3;

import java.util.ArrayList;

public class ArrayMain {

    static ArrayList<Integer> list = new ArrayList<>();

    public ArrayMain(){
    }

    public static void FillArray(int N, int M){

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                list.add(i + j);

            }
        }
    }

    public static void PrintArrMain(int N){

        int k = 0;

        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
            k++;

            if (k == N ){
                System.out.println();
                k = 0;
            }
        }

    }
}
