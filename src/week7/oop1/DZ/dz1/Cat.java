package week7.oop1.DZ.dz1;

public class Cat {
    public Cat(){
    }
    private void sleep(){
        System.out.println("Sleep");
    }
    private void meow(){
        System.out.println("Meow");
    }
    private void eat(){
        System.out.println("Eat");
    }
    public void status(){
        double n = Math.random();
        if (n < 0.3){
            sleep();
        }
        else if (n > 0.3 && n < 0.6){
            meow();
        }
        else {
            eat();
        }
    }
}
