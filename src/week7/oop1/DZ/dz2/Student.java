package week7.oop1.DZ.dz2;

public class Student {
    private String name = "";
    private  String surname = "";
    private int[] grades = new int[10];

    public Student(){
    }
    public Student(String newName, String newSurname){
        name = newName;
        surname = newSurname;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String newName) {
        this.name = newName;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String newSurname) {
        this.surname = newSurname;
    }

    public int[] getGrades() {
        return this.grades;
    }

    public void setGrades(int[] newGrades) {
        if (newGrades.length <= 10) {
            this.grades = newGrades;
        }
        else {
            System.out.println("Количество оценок у студента " + "не должно превышать 10 ");
        }
    }
// переставляю все оценки на один влево и добавляю оценку в конец
    public void updateGrades(int n){
        for (int i = 0; i < 9; i++) {
            grades[i]= grades[i + 1];
        }
        grades[9] = n;
    }
    // среднее арифметическое
    public double averageGrades(){
        double sum = 0;
        double k = 0;
        for (int i = 0; i < grades.length; i++) {
            sum += grades[i];
            if (grades[i] > 0){
                k++;
            }
        }
        double sum1 = (sum / k) * 10 / 10.0;
        return sum1;
    }



}
