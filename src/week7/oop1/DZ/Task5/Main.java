package week7.oop1.DZ.Task5;

import week7.oop1.DZ.Task4.TimeUnit;

public class Main {
    public static void main(String[] args) {

        DayOfWeek[] arr = new DayOfWeek[7];


        arr[0] =  DayOfWeek.day1;
        arr[1] =  DayOfWeek.day2;
        arr[2] =  DayOfWeek.day3;
        arr[3] =  DayOfWeek.day4;
        arr[4] =  DayOfWeek.day5;
        arr[5] =  DayOfWeek.day6;
        arr[6] =  DayOfWeek.day7;

        for (int i = 0; i < 7; i++) {
            System.out.println(arr[i].getN() + " " +  arr[i].getS());
        }
    }
}
