package week9.oop3.dz.task4;

public class Dog {

    private  String nickName = "";
  private double averageRating = 0;
  Participant participant = new Participant();


    Dog (){
  }
    Dog(String nickName){
        this.nickName = nickName;
    }

    Dog(double averageRating ) {
        this.averageRating = averageRating;
    }

    Dog(Participant participant ){
        this.participant = participant;
    }

    Dog(String nickName,double averageRating){
        this.nickName = nickName;
        this.averageRating = averageRating;
}
    Dog(double averageRating, Participant participant ){
        this.averageRating = averageRating;
        this.participant = participant;
    }
    Dog(String nickName,double averageRating, Participant participant ){
        this.nickName = nickName;
        this.averageRating = averageRating;
        this.participant = participant;
    }

    public String getNickName() {
        return nickName;
    }

    public double getAverageRating(){
        return averageRating;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public static Dog[] averageRating(Dog[] arrDog, int[][] arrEvaluations){

        double sum1 = 0;
        int averageRating = 0;

        for (int i = 0; i < arrDog.length; i++) {
            for (int j = 0; j < 3; j++) {
                sum1 += arrEvaluations[i][j];
            }
            averageRating = (int)((sum1/3)* 10.0);
            arrDog[i].setAverageRating(averageRating/ 10.0) ;
            sum1 = 0;
        }
        return arrDog;
    }
    public static Dog[] maxSort(Dog[] arrDog) {
        Dog[] first = new Dog[1];

        for (int i = 0; i < arrDog.length; i++) {
            for (int j = 0; j < arrDog.length; j++) {
                first[0] = arrDog[i];
                if (arrDog[i].getAverageRating() < arrDog[j].getAverageRating()) {
                    arrDog[i] = arrDog[j];
                    arrDog[j] = first[0];
                }
            }
        }
        return arrDog;
    }

}
