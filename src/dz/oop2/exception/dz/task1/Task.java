package dz.oop2.exception.dz.task1;

import dz.oop2.exception.dz.task2.MyUncheckedException;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) throws MyCheckedException {
        Scanner scanner = new Scanner(System.in);

        try{
            myPrint(scanner.nextInt());

        }
        catch (Exception ex) {
            throw new MyCheckedException("проверяемое");
        }
    }

    private static void myPrint(int s) {
        System.out.println(s);
    }
}
