package dz.oop2.exception.dz.task4;

public class MyEvenNumber {

    int n = 0;

    MyEvenNumber(){
    }

    MyEvenNumber(int n){
        setN(n);
    }

    public int getN() {
        return n;
    }

    public void setN(int n) throws IllegalArgumentException{
       if (n % 2 == 0) {
           this.n = n;
       }
       else {
           throw new IllegalArgumentException(" число n должно быть четным");
       }
    }
}
