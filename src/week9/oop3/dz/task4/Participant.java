package week9.oop3.dz.task4;

public class Participant {
    private String name = "";
    Participant(){
    }
    Participant(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
       this.name = name;
    }
}
