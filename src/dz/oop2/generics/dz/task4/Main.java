package dz.oop2.generics.dz.task4;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Document document = new Document(1,"qwer",12);
        Document document1 = new Document(2,"qwer2",123);

        ArrayList<Document> list = new ArrayList<>();
        list.add(document);
        list.add(document1);

        Map<Integer,Document> documentMap = new HashMap<>();

        documentMap = organizeDocuments(list);

        System.out.println(documentMap.get(1).name);
        System.out.println(documentMap.get(2).name);

    }
    
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> hashMap = new HashMap<>();
        for (int i = 0; i < documents.size() ; i++) {
            hashMap.put(documents.get(i).id, documents.get(i));
        }
        return hashMap;
    }
}
