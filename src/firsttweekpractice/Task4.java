package firsttweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        double P = 3.14;
        // диаметр
        double d = Math.sqrt(s * 4 / Math.PI);
        // длина окружности
        double l = Math.sqrt(s * 4 * Math.PI);
        // так же есть есть закономерность Пи = L/d, можно вычеслить через нее
        System.out.println(" Результат работы программы диаметр " + d);
        System.out.println(" Результат работы программы длина окружности " + l);
    }
}