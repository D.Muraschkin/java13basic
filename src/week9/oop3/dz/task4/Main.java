package week9.oop3.dz.task4;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        // заполнение массива
        Dog[] arrDog = new Dog[n];
        for (int i = 0; i < n; i++) {
            Participant participant = new Participant();
            participant.setName(scanner.next());
            Dog dog = new Dog(participant);
            arrDog[i] = dog;
        }

        for (int i = 0; i < n; i++) {
            arrDog[i].setNickName(scanner.next());
        }

        int[][] arrEvaluations = new int[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                arrEvaluations[i][j] = scanner.nextInt();
            }
        }

        arrDog = Dog.averageRating(arrDog, arrEvaluations);

//    Сортировка массива от min к max оценке
        arrDog = Dog.maxSort(arrDog);

//        вывод трех победителей
        for (int i = arrDog.length - 1; i > arrDog.length - 4; i--) {
            System.out.println(arrDog[i].participant.getName() + ": " + arrDog[i].getNickName() + ", " + arrDog[i].getAverageRating());

        }

    }
}

