package firsttweekpractice;

import java.util.Scanner;

public class Tast5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();//2
        int b = scanner.nextInt();//5

        System.out.println(" Входные данные");
        System.out.println("a= " + a + "; b= " + b);

        //логика
        a = a + b;//2  + 5 = 7
        b = a - b;// 7 - 5 = 2
        a = a - b;// 7 - 2 = 5
        System.out.println("Результат");
        System.out.println("a= " + a + "; b= " + b);
    }
}
