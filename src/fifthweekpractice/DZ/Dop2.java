package fifthweekpractice.DZ;

import java.util.Scanner;

public class Dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        square(arr);
        int[] arr3 = square(arr);
        sort(arr3);

    }
    // возводим в квадрат
    public static int[] square(int[] arr1){
        int[] arr2 = new int[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            arr2[i] = arr1[i] * arr1[i];
        }
        return arr2;
    }
    // сортирование и выведение
    public static void sort(int[] arr1){

        for (int k = 0; k < arr1.length; k++) {
            for (int i = 0; i < arr1.length; i++) {
                int first = arr1[k];
                if (arr1[k] < arr1[i]) {
                    arr1[k] = arr1[i];
                    arr1[i] = first;
                }
            }
        }
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
    }

}


