package dz.oop2.exception.dz.task3;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");
        overwriting(file);

    }

    public static void overwriting(File newFile) throws IOException{

       try ( FileReader reader = new FileReader(newFile);
             FileWriter writer = new FileWriter("output.txt")) {

           int symbol = reader.read();

           while (symbol != -1) {
               if (symbol > 97 && symbol < 128) {
                   writer.write((char) (symbol - 32));
               } else {
                   writer.write((char) symbol);
               }
               symbol = reader.read();
           }
       }

    }

}
