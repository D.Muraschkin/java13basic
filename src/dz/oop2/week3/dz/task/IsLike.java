package oop2.week3.dz.task;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface IsLike {
    boolean meaning()  ;
}
