package dz.oop2.week4.dz.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Multiply {
    public static void main(String[] args) {

        List<Integer> list = List.of(1,2,3,4,5);


        Optional<Integer> list1 = list.stream()
                .reduce((s1,s2) -> s1 * s2);

        System.out.println(list1);

    }
}
