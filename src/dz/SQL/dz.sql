create table product
(
    flowers varchar(50) unique ,
    price int
);
insert into product
    values('роза', 100);
insert into product
    values('лилия', 50);
insert into product
    values('ромашка', 25);

select * from product;

drop table product;
----------------------------------

create table client
(
    id serial primary key,
    name varchar(50) not null,
    phone varchar(50) not null
);
insert into client (name, phone)
       values ('Anton','+7 999 123 4567');
insert into client (name, phone)
       values ('Andrei','+7 999 123 9876');
insert into client (name, phone)
       values ('Stepan','+7 999 123 4556');
insert into client (name, phone)
       values ('Semen','+7 999 123 2558');

 select * from client;
drop table client;
---------------------------------------------
create table orders
(
    id serial primary key,
    client_id integer references client (id),
    flowers  varchar CHECK (flowers IN ('роза','лилия','ромашка')) references product (flowers),
    quantity integer CHECK ( quantity > 0 and quantity < 100 ) not null,
    date timestamp not null
);

insert into orders(client_id, flowers, quantity, date)
            values(1,'роза',5,now() - interval '30 day');
insert into orders(client_id, flowers, quantity, date)
            values(1,'роза',2,now());
insert into orders(client_id, flowers, quantity, date)
            values(2,'лилия',4,now());
insert into orders(client_id, flowers, quantity, date)
            values(3,'ромашка',3,now());
insert into orders(client_id, flowers, quantity, date)
            values(4,'роза',2,now());

select * from orders;

--task1----------------------------------------------
select *
from orders o join client c on o.client_id = c.id
where o.id = 1;

--task2----------------------------------------------
select *
from orders o join client c on o.client_id = c.id
where c.id = 1 and date >= now() - interval '1 month' ;

--task3----------------------------------------------
select max(orders.quantity) , orders.flowers
from orders
group by orders.flowers limit 1;

--task4----------------------------------------------
select sum(o.quantity * p.price)
from orders o join product p on o.flowers = p.flowers;


drop table orders;
