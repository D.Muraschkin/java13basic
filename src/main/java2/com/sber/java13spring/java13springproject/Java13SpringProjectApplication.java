package com.sber.java13spring.java13springproject;

import com.sber.java13spring.java13springproject.dbexample.MyDBApplicationContext;
import com.sber.java13spring.java13springproject.dbexample.dao.BookDao;
//import com.sber.java13spring.java13springproject.dbexample.dao.BookDaoJDBC;
import com.sber.java13spring.java13springproject.dbexample.dao.UserDAO;
import com.sber.java13spring.java13springproject.dbexample.model.Book;
import com.sber.java13spring.java13springproject.dbexample.model.UserDZ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Java13SpringProjectApplication
      implements CommandLineRunner {
    private BookDao bookDao;

    @Autowired
    public void setBookDaoBean(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    
    public static void main(String[] args) {
        SpringApplication.run(Java13SpringProjectApplication.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {


//  --------------------------------------------------------
          ApplicationContext ctx = new AnnotationConfigApplicationContext (MyDBApplicationContext.class);
          UserDAO userDAO = ctx.getBean(UserDAO.class);
//  ----------------------№1--------------------------------
        userDAO.dropTableUser();
        userDAO.createTableUser();

        
// ----------------------№3---------------------------------
        UserDZ user = new UserDZ();
        user.setSurName("qwert");
        user.setName("asdf");
        user.setDateOfBirth("2020");
        user.setPhone("123456");
        user.setMail("@ty");
        user.setListBook("Недоросль,Доктор Живаго");

      userDAO.createUsers(user);

// ---------------------№4----------------------------------

        String[] arr  =  userDAO.informationBook("123456");

        List<Book> list = bookDao.findBookByTitle(arr);
        for (Book book : list){
            System.out.println(book);
        }
    }
}
