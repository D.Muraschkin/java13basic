package oop2.week3.dz.task;

public class AnnotationReflection {
    public static void main(String[] args) {
        printAnnotation(TestIsLike.class);

    }
    public static void printAnnotation(Class<?> cls){
        if(!cls.isAnnotationPresent(IsLike.class)){
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println(isLike.meaning());
    }
}
