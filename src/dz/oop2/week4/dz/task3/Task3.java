package dz.oop2.week4.dz.task3;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {


        List<String> list = List.of("abc", "", "", "def", "qqq");
       int y = (int) list.stream()
               .filter(x -> x != "")
               .count();

        System.out.print(y);
    }

}
