package week10.dz;

public interface Fish extends Animal{

    @Override
    public default void eat(){
        Animal.super.eat();
    }

    @Override
    public default void sleep(){
        Animal.super.sleep();
    }

    @Override
    public default void wayOfBirth() {
        System.out.println("мечут икру");
    }

    @Override
    public void methodOfTransportation();
}
