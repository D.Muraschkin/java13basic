package dz.oop2.week4.dz.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class Test {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            list.add(i);
        }


       Optional<Integer> list1 = list.stream()
                .filter(x -> x % 2 == 0)
                .reduce((s1,s2) -> s1 + s2);

        System.out.println(list1);

    }

}
