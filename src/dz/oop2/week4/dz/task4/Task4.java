package dz.oop2.week4.dz.task4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {

        List<Float> list = new ArrayList<>();
        for (int i = 10; i >= 0; i--) {
            list.add((float) i);
        }
        list.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(x -> System.out.print(x + " "));
    }
}
