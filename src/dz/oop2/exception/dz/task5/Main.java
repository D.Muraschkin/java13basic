package dz.oop2.exception.dz.task5;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
        public static void main(String[] args) throws Exception {
            try {
                int n = inputN();
                System.out.println("Успешный ввод!");
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
        private static int inputN() throws Exception {
            try {
                System.out.println("Введите число n, 0 < n < 100");
                Scanner scanner = new Scanner(System.in);
                int n = scanner.nextInt();
                if (n > 100 && n < 0) {
                    throw new Exception("Неверный ввод");
                }
                return n;
            }
            catch (InputMismatchException ex){
                throw new InputMismatchException("Неверный ввод");
            }

            }
        }

