package dz.oop2.generics.dz.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(0);
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(2);

        ArrayList<Integer> uniqueList = new ArrayList<>(unique(list));
        System.out.println(uniqueList);

        ArrayList<String> list1 = new ArrayList<>();
        list1.add("q");
        list1.add("q");
        list1.add("w");
        list1.add("w");
        list1.add("e");
        list1.add("e");

        ArrayList<String> uniqueList1 = new ArrayList<>(unique(list1));
        System.out.println(uniqueList1);
    }

    public static <T> Set<T> unique(ArrayList<T> arr ){

        Set<T> uniqueSet = new HashSet<>(arr);
        return uniqueSet;

    }

}
