package dz.oop2.generics.dz.task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet<T> {


    public static <T> Set<T>  intersection(Set<T> set1, Set<T> set2) {
        Set<T>setIntersection = new HashSet<>();
        setIntersection.addAll(set1);
        setIntersection.retainAll(set2);

    return setIntersection;
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2){
        Set<T>setUnion = new HashSet<>();
        setUnion.addAll(set1);
        setUnion.addAll(set2);

    return  setUnion;
    }


    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2){
        Set<T>setRelativeComplement = new HashSet<>();
        setRelativeComplement.addAll(set1);
        setRelativeComplement.removeAll(set2);

        return setRelativeComplement;
    }


}
