package secondweekpractice.DZ;

import java.util.Scanner;

public class DOP1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String x = scanner.next();
        int y = x.length();

        if (y > 7) {
            char a1 = x.charAt(0);
            char a2 = x.charAt(1);
            char a3 = x.charAt(2);
            char a4 = x.charAt(3);
            char a5 = x.charAt(4);
            char a6 = x.charAt(5);
            char a7 = x.charAt(6);
            char a8 = x.charAt(7);

            if (((a1 >= 'a' && a1 <= 'z') || (a2 >= 'a' && a2 <= 'z') || (a3 >= 'a' && a4 <= 'z') || (a5 >= 'a' && a5 <= 'z') || (a6 >= 'a' && a6 <= 'z') || (a7 >= 'a' && a7 <= 'z') || (a8 >= 'a' && a8 <= 'z')) &&
                    ((a1 >= 'A' && a1 <= 'Z') || (a3 >= 'A' && a3 <= 'Z') || (a4 >= 'A' && a4 <= 'Z') || (a5 >= 'A' && a5 <= 'Z') || (a6 >= 'A' && a6 <= 'Z') || (a7 >= 'A' && a7 <= 'Z') || (a8 >= 'A' && a8 <= 'Z')) &&
                    ((a1 >= '0' && a1<= '9') || (a2 >= '0' && a2<= '9') || (a3 >= '0' && a3<= '9') || (a4 >= '0' && a4<= '9') || (a5 >= '0' && a5<= '9') || (a6 >= '0' && a6 <= '9') || (a7 >= '0' && a7 <= '9') || (a8 >= '0' && a8 <= '9')) &&
                    (((a1 >= '!' && a1<= '/') || (a2 >= '!' && a2<= '/') || (a3 >= '!' && a3<= '/') || (a4 >= '!' && a4<= '/') || (a5 >= '!' && a5<= '/') || (a6 >= '!' && a6 <= '/') || (a7 >= '!' && a7 <= '/') || (a8 >= '!' && a8 <= '/')) ||
                            ((a1 >= '[' && a1<= '_') || (a2 >= '[' && a2<= '_') || (a3 >= '[' && a3<= '_') || (a4 >= '_' && a4<= '_') || (a5 >= '[' && a5<= '_') || (a6 >= '[' && a6 <= '_') || (a7 >= '[' && a7 <= '_') || (a8 >= '[' && a8 <= '_')) ||
                            ((a1 >= ':' && a1<= '?') || (a2 >= ':' && a2<= '?') || (a3 >= ':' && a3<= '?') || (a4 >= ':' && a4<= '?') || (a5 >= ':' && a5<= '?') || (a6 >= ':' && a6 <= '?') || (a7 >= ':' && a7 <= '?') || (a8 >= ':' && a8 <= '?'))))
            {
                System.out.println("пароль надежный");
            }
            else  {
                System.out.println("пароль не прошел проверку");
            }

        }
        else {
            System.out.println("пароль не прошел проверку");
        }

    }
}