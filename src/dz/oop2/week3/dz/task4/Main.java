package dz.oop2.week3.dz.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Class<?>> result = getAllInterfaces(ArrayList.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {

        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            for(Class<?> anInterface : cls.getInterfaces()){
                interfaces.add(anInterface);
                Class<?>[] arrayInterface = anInterface.getInterfaces();
                while (arrayInterface.length > 0){
                    for (Class<?> elementInterfaces : arrayInterface){
                        anInterface = elementInterfaces;
                        interfaces.add(anInterface);
                        arrayInterface = anInterface.getInterfaces();
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
   }
}
