package fifthweekpractice.DZ;

import java.util.Scanner;

public class Dop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n =0;
        int x = 0;

        while (true){
            System.out.println("Введите число N ");
            n = scanner.nextInt();
            if (n < 8){
                System.out.println("Пароль с N количеством символов небезопасен");
            }
            else {
                char[] arr = new char[n];
                int i = 0;

                while (i < n){
                    x = (int) (Math.random() * 200);
                    if ((x > 48 && x < 59) && i == 0){
                        arr[i++] = (char) x;
                    }
                    else if ((x > 64 && x < 91) && i == 1) {
                        arr[i++] = (char) x;
                    }
                    else if ((x > 96 && x < 123) && i == 2) {
                        arr[i++] = (char) x;
                    }
                    else if ((x == 95 || x == 42 || x == 45) && i == 3){
                        arr[i++] = (char) x;
                    }
                    else if (((x > 96 && x < 123) || (x > 48 && x < 59) || (x > 64 && x < 91) || x == 95 || x == 42 || x == 45) && i > 3) {
                        arr[i++] = (char) x;
                    }
                }
                for ( i = 0; i < n; i++) {
                    System.out.print(arr[i] + " ");
                }
                break;
            }
        }
    }
}

