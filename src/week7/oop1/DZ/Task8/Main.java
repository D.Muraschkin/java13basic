package week7.oop1.DZ.Task8;

public class Main {
    public static void main(String[] args) {

        //создается объект , и задаются курсы валют
        Atm rt = new Atm(5, 0.2);

        //конвертация
        rt.rubInDol(5);
        rt.dolInRub(5);

        //количество созданных объектов
        System.out.println(rt.count());
    }
}
