package oop2.week3.dz.task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {

        printData(APrinter.class, 5);

    }

    public static void printData(Class<?> myClass, int x){
        try {
            Method method = myClass.getDeclaredMethod("print",int.class);
            method.setAccessible(true);
            method.invoke(new APrinter(),x);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
