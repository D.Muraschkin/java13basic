package week8.oop2.dz;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = "";

        Library library = new Library();

        MainBook book2 = new MainBook("Гарри Поттер и узник Азкабана" , "Джоан Роулинг");
        MainBook book3 = new MainBook("Мастер и Маргарита", "Михаил Булгаков");
        MainBook book4 = new MainBook("Собачье сердце", "Михаил Булгаков");
        MainBook book5 = new MainBook("Двенадцать стульев", "Илья Ильф, Евгений Петров");
        MainBook book6 = new MainBook("Мёртвые души", "Николай Гоголь");
        MainBook book7 = new MainBook("Граф Монте-Кристо", "Александр Дюма");
        MainBook book8 = new MainBook("Золотой теленок", "Илья Ильф, Евгений Петров");
        MainBook book9 = new MainBook("Три товарища", "Эрих Мария Ремарк");


        library.addArrayLibrary(book2);
        library.addArrayLibrary(book3);
        library.addArrayLibrary(book4);
        library.addArrayLibrary(book5);
        library.addArrayLibrary(book6);
        library.addArrayLibrary(book7);
        library.addArrayLibrary(book8);
        library.addArrayLibrary(book9);





        while (true) {
            System.out.println("Введите имя: ");

            String input = scanner.nextLine();

            Visitor visitor = new Visitor(input);
            Visitor visitor1 = new Visitor();
            boolean matched = false;

            Visitor[] arrVisitor = new Visitor[10];
            int numberOfVisitor = 0;
            int numberOfVisitorArr = 0;


            for (int i = 0; i < arrVisitor.length; i++) {
                if (arrVisitor[i] != null) {
                    if (arrVisitor[i].getName().equals(input)){
                        matched = true;
                        numberOfVisitorArr = i;
                        System.out.println(numberOfVisitorArr);
                    }
                }
            }
            if (matched != true) {

                if (numberOfVisitor == arrVisitor.length) {
                    int n1 = numberOfVisitor;
                    n1++;
                    arrVisitor = Arrays.copyOf(arrVisitor, n1);
                }

                arrVisitor[numberOfVisitor] = visitor;
                numberOfVisitorArr = numberOfVisitor;
                numberOfVisitor++;
                System.out.println(arrVisitor[numberOfVisitorArr].getName());
            }




            System.out.println("1 - Добавить книгу");
            System.out.println("2 - Удалить книгу");
            System.out.println("3 - Вернуть книгу по названию");
            System.out.println("4 - Вернуть список книг по автору");
            System.out.println("5 - Одолжить книгу");
            System.out.println("6 - Выйти");

            String input1 = scanner.nextLine();


            if (input1.equals("1")) {
                while (true) {
                    System.out.println("1 - Добавить книгу");
                    System.out.println("2 - Выйти");

                    String n = scanner.nextLine();

                    if (n.equals("1")) {
                        System.out.print("Введите название книги: ");
                        String s1 = scanner.nextLine();

                        System.out.print("Введите автора книги: ");
                        String s2 = scanner.nextLine();

                        MainBook book = new MainBook(s1, s2);
                        boolean a = library.comparisonBook(book);
                        if (a == true) {
                            System.out.println("Такая книга существует в библиотеке");
                        } else {
                            library.addArrayLibrary(book);
                            System.out.println("Добавленна");
                        }
                    } else {
                        break;
                    }
                }
            }
            if (input1.equals("2")) {
                while (true) {
                    System.out.println("1 - Удалить");
                    System.out.println("2 - Выйти");
                    String n = scanner.nextLine();
                    if (n.equals("1")) {
                        System.out.print("Введите название книги: ");
                        String s1 = scanner.nextLine();

                        System.out.print("Введите автора книги: ");
                        String s2 = scanner.nextLine();

                        MainBook book = new MainBook(s1, s2);
                        boolean a = library.comparisonBook(book);
                        if (a == true) {
                            library.remove(book);
                            System.out.println("Книга удалена");
                        } else {
                            System.out.println("Такой книги не существует");
                        }
                    } else {
                        break;
                    }
                }
            }
            if (input1.equals("3")){
                while (true) {
                    System.out.println("1 - Вернуть книгу по названию");
                    System.out.println("2 - Выйти");
                    String n = scanner.nextLine();
                    int n1 = 0;

                    if (n.equals("1")) {
                        System.out.print("Введите название книги: ");
                        String s1 = scanner.nextLine();

                          boolean a = library.comparisonBookTitle(s1);
                        if (a == true ) {
                            System.out.println(arrVisitor[0].getIdVisitor());

                            System.out.println(library.getBookStatus(s1) +" "+ arrVisitor[numberOfVisitorArr].getIdVisitor());
                             if (library.getBookStatus(s1) == arrVisitor[numberOfVisitorArr].getIdVisitor()) {
                                 library.returnBookTitle(s1);
                                 System.out.println("Книга возващена");
                             }else {
                                 System.out.println("Книга одолжена не вами");
                             }
                        } else {
                            System.out.println("Такой книги не существует");
                        }

                    } else {
                        break;
                    }
                }
            }
            if (input1.equals("4")){
                while (true) {
                    System.out.println("1 -  Вернуть список книг по автору");
                    System.out.println("2 - Выйти");
                    String n = scanner.nextLine();
                    if (n.equals("1")) {
                        System.out.print("Введите Автора книг: ");
                        String s1 = scanner.nextLine();

                       boolean a = library.comparisonBookAuthor(s1);
                        if (a == true ) {
                            library.returnBookAuthor(s1);
                            System.out.println("Книги  возващены");
                        } else {
                            System.out.println("Книг с таким автором не существует ");
                        }

                    } else {
                        break;
                    }
                }
            }
            if (input1.equals("5")){
            while (true) {
                System.out.println("1 - Одолжить книгу");
                System.out.println("2 - Выйти");
                String n = scanner.nextLine();
                if (n.equals("1")) {
                    System.out.print("Введите название книги: ");
                    String s1 = scanner.nextLine();

                    boolean a = library.comparisonBookTitle(s1);
                    if (a == true ) {
                        if (arrVisitor[numberOfVisitorArr].getIdVisitor() == 0 ){
                            if (library.getBookStatus(s1) == 1){
                                while (true) {
                                    int idBook = (int) (Math.random() * 1000);
                                    if ( idBook > 1) {
                                        arrVisitor[numberOfVisitorArr].setIdVisitor(idBook);
                                        library.setBookStatus(s1, idBook);
                                        System.out.println("Вы одолжили книгу");

                                        break;
                                    }
                                }
                            }
                            else {
                                System.out.println("Книга с таким названием одолжена другим читателем");
                            }
                        }
                        else {
                            System.out.println("У вас уже есть книга которую Вы одолжили");
                        }

                    }
                    else {
                        System.out.println("Книг с таким названием не существует");
                    }

                } else {
                    break;
                }
            }
            }

            if (input1.equals("6")){
                break;
            }
        }

    }
}
