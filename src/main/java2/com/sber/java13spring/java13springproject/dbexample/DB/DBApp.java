package com.sber.java13spring.java13springproject.dbexample.DB;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.java13spring.java13springproject.dbexample.constants.DBConsts.*;

public enum DBApp {
    INSTANCE;
    private Connection connection;
    public Connection newConnection() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                    USER, PASSWORD);
        }
        return connection;
    }
}
