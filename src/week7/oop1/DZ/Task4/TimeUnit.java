package week7.oop1.DZ.Task4;

import java.util.Date;

public class TimeUnit {
int h, m, s;

    public TimeUnit(int n1, int n2, int n3){
        if(n1 < 24 && n2 < 60 && n3 < 60) {
        h = n1;
        m = n2;
        s = n3;
        }
        else {
            System.out.println("Введен не корректный формат времени(значение не должно превышать часы  23, минуты 59, секунды 59)");
        }
    }
    public TimeUnit(int n1, int n2){
        if(n1 < 24 && n2 < 60) {
            h = n1;
            m = n2;
            s = 0;
        }
         else {
                System.out.println("Введен не корректный формат времени(значение не должно превышать часы  23, минуты 59, секунды 59)");
            }
    }
    public TimeUnit(int n1){
            if(n1 < 24) {
                h = n1;
                m = 0;
                s = 0;
            }
            else {
                System.out.println("Введен не корректный формат времени(значение не должно превышать часы  23, минуты 59, секунды 59)");
            }
    }
    public TimeUnit(){
        h = 0;
        m = 0;
        s = 0;
    }

    public  void outputTime() {
        // Подгоняю под формат 00:00:00, если меньше 10 добавляю перед цифрой 0
        int n = 0;
        if(h < 10)
            System.out.print(n +""+ h);
        else
            System.out.print(h);
        if (m < 10)
            System.out.print(":"+ n +""+ m);
        else
            System.out.print(":" + m);
        if (s < 10)
            System.out.println(":"+ n + "" + s );
        else
            System.out.println(":" + s);
    }
    public void sum(int hours, int min, int sec){

        // Перевожу текущее время в секунды
            int currentTime = ((h * 60 * 60) + (m * 60) + s);

            // Перевожу переданное время в секунды и прибавляю к текущему
            int sumTime = ((hours * 60 * 60) + (min * 60) + sec) + currentTime;

            // Вычисляю часы мин. сек.
            int sumHours = sumTime / 60 / 60;
            int sumMin = sumTime / 60 % 60;
            int sumSec = sumTime % 60;

            if (sumHours > 23){
                sumHours %= 24;
            }

            //С оздаю новый объект и присваиваю ему новые значения
            TimeUnit sum1 = new TimeUnit(sumHours, sumMin, sumSec);

            // Вывожу на экран
            sum1.outputTime();

    }
    public void amPm(){

        // Подгон числа под 12 часовой формат
        if ( h > 12){
            int hours = h - 12;
            TimeUnit time = new TimeUnit(hours, m, s);
            time.outputTime();
            System.out.print(" pm");
        }
        else {
            int hours = h;
            TimeUnit time = new TimeUnit(hours, m, s);
            time.outputTime();
            System.out.print("am");
        }
    }
}
