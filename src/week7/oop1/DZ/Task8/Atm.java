package week7.oop1.DZ.Task8;

public class Atm {
    static double kursRub = 0;
    static double kursDol = 0;
    static int counter = 0;

    //проверка что бы курс был строго больше ноля
    Atm(double n1, double n2){
        if (n1 > 0 && n2 > 0){
            this.kursRub = n1;
            this.kursDol = n2;
            counter++;
        }
        else {
            System.out.println("Курс должен быть больше ноля");
        }

    }


    public double getKursRub(){
        return kursRub;
    }
    public double getKursDol(){
        return kursDol;
    }

    public void rubInDol(double rub){
        double dol = rub * kursRub;
       System.out.println("количество долларов после конвертации = " + dol);
    }
    public void dolInRub(double dol) {
        double rub = dol * kursDol;
        System.out.println("количество рублей после конвертации = " + rub);
    }
    static int count(){
        return counter;
    }
}
