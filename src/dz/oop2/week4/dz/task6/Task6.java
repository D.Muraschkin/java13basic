package dz.oop2.week4.dz.task6;

import java.util.*;

public class Task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> set = Set.of(
                Collections.emptySet(),
                new HashSet<>(Arrays.asList(0, 1, 2, 3)),
                new HashSet<>(Arrays.asList(4, 5, 6)),
                new HashSet<>(Arrays.asList(10, 0, 100, 1000)),
                new HashSet<>(Arrays.asList(25, 0, 1, 16, 4, 9)),
                new HashSet<>(Arrays.asList(0, 5, 50))
        );

        Set<Integer> newSet = new HashSet<>();

        set.stream()
                .forEach(
                        x ->{
                            for ( int y : x){
                                newSet.add(y);
                            }
                        }
                );
        newSet.stream()
                .forEach(System.out::println);
    }
}
