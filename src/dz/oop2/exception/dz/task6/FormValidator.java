package dz.oop2.exception.dz.task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;


public class FormValidator {
    private static final Pattern NAME_PATTERN = Pattern.compile("([A-Z][a-z]{1,19})");
    private static final Pattern BIRTHDATE_PATTERN = Pattern.compile("^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}");


    public static void checkName(String str){
        if ( NAME_PATTERN.matcher(str).matches()){
            System.out.println("super");
        }
        else {
            throw new IllegalArgumentException(" длина имени должна быть от 2 до 20 символов, первая буква заглавная");
        }
    }
    public static void checkBirthdate(String str) throws ParseException {
        if (BIRTHDATE_PATTERN.matcher(str).matches()) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date yearOfBirthInt = format.parse(str);

            if (yearOfBirthInt.compareTo(date) <= 0 && yearOfBirthInt.getYear() >= 0){
                System.out.println("super дата рождения");
            }
            else {
                throw new IllegalArgumentException(" дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
            }
        } else {
            throw new IllegalArgumentException("формат даты должен быть 01.01.1900");
        }
    }

    public static void checkGender(String str){
        try{
            Gender.valueOf(str);
            System.out.println("super Gender");
        }
        catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException(" пол должен быть  Male или Female");
        }
    }

        public static void checkHeight(String str){
                try{
                    double hieght = Double.parseDouble(str);
                    if (hieght > 0 && hieght < 300) {
                        System.out.println("super рост");
                    }
                    else {
                        throw new IllegalArgumentException(" число жолжно быть ,больше 0 и меньше 300");
                    }
                }
                catch (IllegalArgumentException ex){
                    throw new IllegalArgumentException("некорректный формат");
                }
            }



}

