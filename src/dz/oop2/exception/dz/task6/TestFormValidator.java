package dz.oop2.exception.dz.task6;

import java.text.ParseException;

public class TestFormValidator {
    public static void main(String[] args) {
        FormValidator formValidator = new FormValidator();

        try{
            formValidator.checkName("Aqwert");
            formValidator.checkBirthdate("03.12.1901");
            formValidator.checkGender("Male");
            formValidator.checkHeight("");
        }
        catch( IllegalArgumentException ex){
            System.out.println(ex);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }


    }
}
