package secondweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 999 && n < 10000) {
            int end = n % 10;
            int start = n / 1000;
            if (end != start) {
                System.out.println("не палиндром");
            } else {
                System.out.println("end = n % 10 = " + (n % 10));
                System.out.println("start = n / 1000 = " + (n / 1000));
                end = (n % 100) / 10;
                start = (n / 100) % 10;

                if (end != start) {
                    System.out.println("не палиндром");
                } else {
                    System.out.println("палиндром");
                }
            }
        }
                    else {
                    System.out.println("Ввели некоректное число (4 знака должно быть)");
                }

    }
}