package week10.dz;

public class Bat implements Mammal{
    @Override
    public void eat() {
        Mammal.super.eat();
    }

    @Override
    public void sleep() {
        Mammal.super.sleep();
    }

    @Override
    public void wayOfBirth() {
        Mammal.super.wayOfBirth();
    }

    @Override
    public void methodOfTransportation() {
        System.out.println("летает медленно");

    }
}
