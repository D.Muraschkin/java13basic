package com.sber.java13spring.java13springproject.dbexample.dao;

import com.sber.java13spring.java13springproject.dbexample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//import static sun.nio.fs.Util.split;

@Component
public class BookDao {
    
    private final Connection connection;
    
    public BookDao(Connection connection) {
        this.connection = connection;
    }

    public List<Book> findBookByTitle(String[] bookTitle) throws SQLException {
        List<Book> listBook = new ArrayList<>();
        for (int i = 0; i < bookTitle.length; i++) {
            String title = bookTitle[i];
            PreparedStatement selectQuery = connection.prepareStatement("select * from books where title = ? ");
            selectQuery.setString(1, title);
            ResultSet resultSet = selectQuery.executeQuery();

            Book book = new Book();
            while (resultSet.next()) {
                book.setBookAuthor(resultSet.getString("author"));
                book.setBookTitle(resultSet.getString("title"));
                book.setDateAdded(resultSet.getDate("date_added"));
                listBook.add(book);
            }
        }
        return listBook;
    }
    
}
