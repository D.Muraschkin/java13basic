package week8.oop2.dz;

import java.util.Arrays;

public class Library {

    private MainBook[] arrLibrary = new MainBook[1];
    private String[] arrAuthor = new String[1];

    private int numberOfBook;


    Library() {
    }

    //добавить книгу в библиотеку и если не хватает в массиве места увеличить на один
    public void addArrayLibrary(MainBook book) {

        if (numberOfBook == arrLibrary.length) {
            int n = numberOfBook;
            n++;
            arrLibrary = Arrays.copyOf(arrLibrary, n);
        }
        arrLibrary[numberOfBook] = book;
        book.setStatus(1);
        numberOfBook++;
    }

    // проверить книгу на совпадение в библиотеке
    public boolean comparisonBook(MainBook book) {
        int n = 0;
        for (int i = 1; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (book.getTitle().equals(arrLibrary[i].getTitle()) && book.getAuthor().equals(arrLibrary[i].getAuthor())) {
                    n = 1;
                }
            }
        }
        if (n != 0){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean comparisonBookTitle(String title) {
        int n = 0;
        for (int i = 1; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (title.equals(arrLibrary[i].getTitle())) {
                    n = 1;
                }
            }
        }
        if (n != 0){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean comparisonBookAuthor(String author) {
        int n = 0;
        for (int i = 0; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (arrLibrary[i].getAuthor().equals(author)) {
                    n = 1;
                }
            }
        }
        if (n != 0){
            return true;
        }
        else{
            return false;
        }
    }


    //удалить книгу из библиотеки
    public void remove(MainBook book) {
        int n = -1;
        for (int i = 0; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (book.getTitle().equals(arrLibrary[i].getTitle()) && book.getAuthor().equals(arrLibrary[i].getAuthor())) {
                    arrLibrary[i] = null;
                    n = i;
                }
            }
        }
            if (n >= 0) {
                for (int i = n - 1; i < arrLibrary.length - 1; i++) {
                    arrLibrary[i] = arrLibrary[i + 1];
                }
                numberOfBook--;
            }

    }

    // вернуть книгу по Названию
    public void returnBookTitle(String title){
        for (int i = 0; i < arrLibrary.length; i++) {
            if (title.equals(arrLibrary[i].getTitle())){
                arrLibrary[i].setStatus(1);
            }
        }
    }
    public int getNumberOfBook(){
        return numberOfBook;
    }
// вернуть все книги по Автору
    public void returnBookAuthor(String name) {
        for (int i = 1; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (name.equals(arrLibrary[i].getAuthor())) {

                    arrLibrary[i].setStatus(1);
                }
            }
        }


    }
    public void lendBook(MainBook book){
        book.setStatus(2);
    }

    public int getBookStatus(String title) {
        int n = 0;
        for (int i = 1; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (title.equals(arrLibrary[i].getTitle())) {
                    n = arrLibrary[i].getStatus();
                }
            }
        }
        return n;
    }
    public void setBookStatus(String title,int id) {
        for (int i = 1; i < arrLibrary.length; i++) {
            if (arrLibrary[i] != null) {
                if (title.equals(arrLibrary[i].getTitle())) {
                     arrLibrary[i].setStatus(id);
                }
            }
        }
    }




}

