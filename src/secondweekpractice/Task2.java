package secondweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str;

        if (n % 2 == 0) {
            if (n >= 0) {
                str = " число четное и больше или равно 0 ";
            } else {
                str = " число четное и меньше 0 ";
            }
        } else {
            str = " число не четное ";
        }


        System.out.println(n + str);
        // тернарный оператор как альтернатива if-tlse
        //  str = (n % 2 == 0) ? " число четное " : " число не четное ";
// if (n % 2 == 0 && n >= 0 ) {
//  System.out.println("четное больше или равно 0");
 //   }
        // if (n % 2 == 0 && n < 0 ) {
       // System.out.println("четное меньше 0");
        System.out.println(n + str);


    }
}


