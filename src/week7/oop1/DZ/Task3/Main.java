package week7.oop1.DZ.Task3;

import week7.oop1.DZ.Task3.StudentService;
import week7.oop1.DZ.dz2.Student;

public class Main {
    public static void main(String[] args) {
        Student stud1 = new Student("Иван", "Иванов");
        Student stud2 = new Student("Степан", "Степнов");
        Student stud3 = new Student("Антон", "Антонов");

        int[] grades1 = {1,5,6,4,8,4,2,3,6,8};
        stud1.setGrades(grades1);
        int[] grades2 = {5,5,3,2,4,1};
        stud2.setGrades(grades2);
        int[] grades3 = {4,5,3,4,5,2};
        stud3.setGrades(grades3);

        Student[] studs = {stud1, stud2, stud3};
        Student best = StudentService.bestStudent(studs);
        System.out.println("Лучший студент: " + best.getSurname() + " " + best.getName() + " " + best.averageGrades());

        StudentService.sortBySurname(studs);
        for (int i = 0; i < studs.length; i++) {
            System.out.println(studs[i].getSurname());
        }
    }
}
