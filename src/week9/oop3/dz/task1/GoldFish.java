package week10.dz;

public class GoldFish implements Fish{
    @Override
    public void eat() {
        Fish.super.eat();
    }

    @Override
    public void sleep() {
        Fish.super.sleep();
    }

    @Override
    public void wayOfBirth() {
        Fish.super.wayOfBirth();
    }

    @Override
    public void methodOfTransportation() {
        System.out.println( "плавает медленно");
    }
}
