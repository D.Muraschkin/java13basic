package week7.oop1.DZ.Task3;
import week7.oop1.DZ.dz2.Student;
import java.util.Arrays;

public class StudentService {

    public static Student bestStudent(Student student[]){
        double max = 0;
        int p = 0;
        for (int i = 0; i < student.length; i++) {
            double y = student[i].averageGrades();
            if (max < y){
                max = y;
                p = i;
            }
        }
        return student[p];
    }

    public static Student[] sortBySurname(Student[] stud){
       String[] arr = new String[stud.length];

       // присваиваю новому массиву фамилии студентов
        for (int i = 0; i < stud.length; i++) {
            arr[i] = stud[i].getSurname();
        }
        // сортирую типа String
        Arrays.sort(arr);

        // сравниваю отсортированный массив с массивом студентов и присваиваю третьему массиву
        int i = 0;
        Student[] stud1 = new Student[stud.length];
        while ( i < stud.length) {
            for (int j = 0; j < stud.length; j++) {
                    if (arr[i].equals(stud[j].getSurname())) {
                        stud1[i] = stud[j];
                        i++;
                        if ( i == stud.length ) {
                            break;
                        }
                    }
            }
        }
        // присваиваю отсортированный массив
        for (int j = 0; j < stud.length; j++) {
            stud[j] = stud1[j];
        }
        return stud;
    }
}
