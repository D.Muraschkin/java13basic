package dz.oop2.week4.dz.task5;

import java.util.ArrayList;
import java.util.List;

public class Task5 {
    public static void main(String[] args) {

        List<String> list = List.of("abc", "def", "qqq");

        String s = String.valueOf(list.size() - 1);

        list.stream()
                .map(x -> x.toUpperCase())
                .forEach(x ->  {
                    System.out.print(x);

                    if (!x.equalsIgnoreCase (list.get(list.size() - 1))){
                        System.out.print(", ");
                    }
                });

    }
}
