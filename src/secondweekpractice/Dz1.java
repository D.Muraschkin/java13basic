package secondweekpractice;

import java.util.Scanner;

public class Dz1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();

        if ((x <= y && x <= z) && (y <= z)){
            System.out.println(z < (x + y));
        }
        else if ((y <= x && y <= z) && (z <= x)){
            System.out.println(x < y + z);
        }
        else {
            System.out.println(y < z + x);
        }
    }
}
